package pt.flag;

/**
 * Created by bruno on 07-01-2017.
 */
public class Machine {
    protected String name = "Machine Type 1";

    public void start() {
        System.out.println("Machine started.");
    }

    public void stop() {
        System.out.println("Machine stopped.");
    }
}
