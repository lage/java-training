package pt.flag;

/**
 * Created by bruno on 07-01-2017.
 */
public class Application {
    public static void main(String[] args) {

        // Create a Person object using the Person class
        Person person1 = new Person();
        person1.name = "Bruno Aguiar";
        person1.age = 27;
        person1.speak();
        person1.sayHello();

        // Create a second Person object
        Person person2 = new Person();
        person2.name = "Sarah Smith";
        person2.age = 30;
        person2.speak();
        person1.sayHello();

        System.out.println(person1.name);
    }
}
