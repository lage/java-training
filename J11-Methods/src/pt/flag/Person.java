package pt.flag;

/**
 * Created by bruno on 07-01-2017.
 */
public class Person {
    // Instance variables (data or "state")
    String name;
    int age;

    // Classes can contain

    // 1. Data
    // 2. Subroutines (methods)

    void speak() {
        System.out.println("My name is: " + name + " and I am " + age + " years old ");
    }

    void sayHello() {
        System.out.println("Hello there!");
    }
}
