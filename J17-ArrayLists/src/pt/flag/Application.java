package pt.flag;

import java.util.ArrayList;

/**
 * Created by bruno on 07-01-2017.
 */
public class Application {
    public static void main(String[] args) {
//        Classes wrapper para primitivas
//        Integer;
//        Long;
//        Character;
//        Boolean;
//        Double;
//        Float;

        ArrayList<Integer> fiboList = new ArrayList<>();

        fiboList.add(0);
        fiboList.add(1);

        for (int i = 2; i < 30; i++) {
            fiboList.add(fiboList.get(i - 1) + fiboList.get(i - 2));
        }

        System.out.println(fiboList);
        System.out.println();

        System.out.println(fiboList.contains(1));
        System.out.println(fiboList.indexOf(17711));
        System.out.println();

        //find max
        int max = Integer.MIN_VALUE;
        for (Integer integer : fiboList) {//for each loop
            if (integer > max) {
                max = integer;
            }
        }
        System.out.println("Max value: " + max);
        System.out.println();
    }
}
