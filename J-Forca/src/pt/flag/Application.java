package pt.flag;

import java.util.Scanner;

/**
 * Created by bruno on 07-01-2017.
 */
public class Application {
    public static void main(String[] args) {

        String[] frases = {
                "Gotham City",
                "Metropolis",
                "Star City",
                "Coast City",
                "Atlantis",
                "New Themyscira",
                "Central City"
        };

        Scanner input = new Scanner(System.in);

        Forca jogoForca = new Forca(frases);

        while (jogoForca.emJogo()) {
            // IMPRIMIR INFO
            System.out.println("vidas: " + jogoForca.getVidas());
            System.out.println(jogoForca.getFraseJogo());

            // PEDIMOS UMA LETRA UTILIZADOR
            System.out.println("Letra?");
            String letra = input.next();
            char l = letra.charAt(0);

            // VERIFICAMOS A LETRA
            jogoForca.verificaLetra(l);

        }


        if (jogoForca.ganhou()) {
            System.out.println(jogoForca.getFraseJogo());
            System.out.println("Ganhaste!!!!");
        } else {
            System.out.println("PERDESTE!!!!");
        }


    }
}