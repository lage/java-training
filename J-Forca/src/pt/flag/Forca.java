package pt.flag;

/**
 * Created by bruno on 07-01-2017.
 */
public class Forca {
    private String fraseSecreta;
    private StringBuilder fraseJogo;

    private int vidas;

    public Forca() {
        vidas = 6;
        fraseSecreta = "pika pika pikachu";
        iniciaJogo();
    }

    public Forca(String fraseSecreta) {
        vidas = 6;
        this.fraseSecreta = fraseSecreta;
        iniciaJogo();
    }

    public Forca(String[] frasesSecretas) {
        vidas = 6;

        int pos = (int) (Math.random() * frasesSecretas.length);
        this.fraseSecreta = frasesSecretas[pos];
        iniciaJogo();
    }

    private void iniciaJogo() {

        fraseJogo = new StringBuilder();

        for (int i = 0; i < fraseSecreta.length(); i++) {
            if (fraseSecreta.charAt(i) == ' ') {
                fraseJogo.append(' ');
            } else {
                fraseJogo.append('-');
            }
        }
    }

    boolean verificaLetra(char letra) {

        boolean existe = false;

        for (int i = 0; i < fraseSecreta.length(); i++) {
            if (fraseSecreta.charAt(i) == letra) {
                fraseJogo.setCharAt(i, letra);
                existe = true;
            }
        }

        if (!existe) {
            vidas--;
        }

        return existe;
    }

    boolean emJogo() {
        return !fraseSecreta.equals(fraseJogo.toString()) && vidas > 0;
    }

    boolean ganhou() {
        return !emJogo() && vidas > 0;
    }

    public String getFraseJogo() {
        return fraseJogo.toString();
    }

    public int getVidas() {
        return vidas;
    }

    @Override
    public String toString() {
        return "OLA OLA OLA EU SOU UMA FORCA!!!!";
    }
}
