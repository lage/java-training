package pt.flag;

/**
 * Class that represents a person
 * <p>
 * Created by bruno on 07-01-2017.
 */
public class Person {
    // Instance variables (data or "state")
    String name;
    int age;


    // Classes can contain

    // 1. Data
    // 2. Subroutines (methods)
}
