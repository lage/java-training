package pt.flag;

/**
 * Created by bruno on 07-01-2017.
 */
public class Application {
    public static void main(String[] args) {
        int value = 0;

        while (value < 10) {
            System.out.println("Hello " + value);

            value = value + 1;
        }


        //exercício tabuada
        //tabuada
        int numTabuada = 1;

        while (numTabuada < 10) {

            System.out.println("Tabuada do " + numTabuada);
            int multiplicador = 0;
            while (multiplicador < 10) {
                multiplicador++;

                int res = numTabuada * multiplicador;
                System.out.println(numTabuada + " X " + multiplicador + " = " + res);
            }

            System.out.println();
            numTabuada++;
        }

    }
}
