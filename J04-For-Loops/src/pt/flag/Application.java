package pt.flag;

/**
 * Ciclos "for"
 * <p>
 * Created by bruno on 07-01-2017.
 */
public class Application {
    public static void main(String[] args) {

        //example
        for (int i = 0; i < 5; i++) {
            System.out.println("The value of i is: " + i);
        }
        System.out.println();

        //exercício tabuada
        for (int i = 1; i <= 10; i++) {

            for (int j = 1; j <= 10; j++) {
                System.out.println(i + " X " + j + " = " + (i * j));

            }
            System.out.println();
        }

        //exercício fibonacci
        int n1 = 0;
        int n2 = 1;

        System.out.println("1: " + n1);
        System.out.println("2: " + n2);

        for (int i = 3; i <= 20; i++) {

            int temp = n1 + n2;
            System.out.println(i + ": " + temp);

            n1 = n2;
            n2 = temp;
        }

    }
}
