package pt.flag;

/**
 * Created by bruno on 07-01-2017.
 */
public class Application {
    public static void main(String[] args) {
        Robot sam = new Robot();

        sam.speak("Hi I'm Sam");
        sam.jump(7);

        sam.move("West", 12.2);

        String greeting = "Hello there";
        sam.speak(greeting);

        int value = 14;
        sam.jump(value);

    }
}
