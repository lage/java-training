package pt.flag;

/**
 * Exemplos de condições e operadores
 * <p>
 * Created by bruno on 07-01-2017.
 */
public class Application {
    public static void main(String[] args) {
        //exemplos operadores
        System.out.println(5 == 5);
        System.out.println(10 != 11);
        System.out.println(3 < 6);
        System.out.println(10 > 100);
        System.out.println();

        //if example
        int idade = 10;
        if (idade >= 18) {
            System.out.println("Toma lá uma cerveja!");
        } else {
            System.out.println("Toma lá uma cola!");
        }
        System.out.println();

        // Using loops with "break":
        int loop = 0;
        while (true) {
            System.out.println("Looping: " + loop);

            if (loop == 3) {
                System.out.println("");
                break;
            }
            loop++;
            System.out.println("Running");
        }
    }
}
