package pt.flag;

/**
 * Created by bruno on 07-01-2017.
 */
public class Application {
    public static void main(String[] args) {
        System.out.println("Hello Java!");


        int myNumber = 88;
        short myShort = 847;
        long myLong = 9797;

        double myDouble = 7.3243;
        float myFloat = 324.3f;

        char myChar = 'y';
        boolean myBoolean = false;

        byte myByte = 127;


        System.out.println(myNumber);
        System.out.println(myShort);
        System.out.println(myLong);
        System.out.println(myDouble);
        System.out.println(myFloat);
        System.out.println(myChar);
        System.out.println(myBoolean);
        System.out.println(myByte);
    }
}
