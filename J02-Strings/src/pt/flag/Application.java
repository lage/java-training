package pt.flag;

/**
 * Created by bruno on 07-01-2017.
 */
public class Application {
    public static void main(String[] args) {
        int myInt = 7;

        String text = "Hello";

        String blank = " ";

        String name = "Bob";

        String greeting = text + blank + name;

        System.out.println(greeting);

        System.out.println("Hello" + " " + "Bob");

        System.out.println("My integer is: " + myInt);

        double myDouble = 7.8;

        System.out.println("My number is: " + myDouble + ".");
    }
}
