package pt.flag;

import java.util.Scanner;

/**
 * Exemplo de input do utilizador atraves de consola
 * <p>
 * Created by bruno on 07-01-2017.
 */
public class Application {
    public static void main(String[] args) {
        System.out.println("Digite um numero: ");

        // Espera que o utilizador insira um valor
        Scanner input = new Scanner(System.in);
        double value = input.nextDouble();
        System.out.println("Valor inserido: " + value);

    }
}
